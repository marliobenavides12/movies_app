import 'package:flutter/widgets.dart';

import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';

import 'package:movies_app/src/data/data_exports.dart';
import 'package:movies_app/src/core/core_exports.dart';
import 'package:movies_app/src/domain/domain_exports.dart';
import 'package:movies_app/src/presentation/logic/bloc_exports.dart';

final sl = GetIt.instance;

Future<void> initDependencies() async {
  // ---------------------------------------------------------------------
  //                                Router
  // ---------------------------------------------------------------------
  sl.registerSingleton<RouterConfig<Object>>((appRouter));

  // ---------------------------------------------------------------------
  //                                 Blocs
  // ---------------------------------------------------------------------

  sl.registerLazySingleton<HomeBloc>(() => HomeBloc(
        getPopularMoviesUseCase: sl(),
        getTopRatedMoviesUseCase: sl(),
      ));
  sl.registerLazySingleton<MovieInfoBloc>(() => MovieInfoBloc(
        getMovieActorsUseCase: sl(),
        getMovieDetailUseCase: sl(),
      ));
  sl.registerLazySingleton<ChangeThemeBloc>(() => ChangeThemeBloc());

  // ---------------------------------------------------------------------
  //                               Use cases
  // ---------------------------------------------------------------------
  sl.registerLazySingleton(() => GetPopularMoviesUseCase(sl()));
  sl.registerLazySingleton(() => GetTopRatedMoviesUseCase(sl()));
  sl.registerLazySingleton(() => GetMovieDetailUseCase(sl()));
  sl.registerLazySingleton(() => GetMovieActorsUseCase(sl()));

  // ---------------------------------------------------------------------
  //                              Repositories
  // ---------------------------------------------------------------------

  sl.registerLazySingleton<Repository>(() => RepositoryImpl(dataSource: sl()));

  // ---------------------------------------------------------------------
  //                               DataSource
  // ---------------------------------------------------------------------

  sl.registerLazySingleton<DataSource>(() => DataSourceImpl(sl()));

  // ---------------------------------------------------------------------
  //                                Dio HTTP
  // ---------------------------------------------------------------------
  sl.registerFactory<Dio>(() => Dio());
}
