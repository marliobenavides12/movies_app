import 'package:dartz/dartz.dart';

import 'package:movies_app/src/domain/domain_exports.dart'
    show Repository, MovieEntity;

class GetPopularMoviesUseCase {
  final Repository _repository;

  GetPopularMoviesUseCase(this._repository);

  Future<Either<String, List<MovieEntity>>> call() async =>
      await _repository.getPopularMovies();
}
