import 'package:dartz/dartz.dart';

import 'package:movies_app/src/domain/domain_exports.dart';

class GetMovieActorsUseCase {
  final Repository _repository;

  GetMovieActorsUseCase(this._repository);

  Future<Either<String, List<MovieActorEntity>>> call(int movieId) async =>
      await _repository.getMovieActors(movieId);
}
