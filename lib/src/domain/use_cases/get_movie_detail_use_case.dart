import 'package:dartz/dartz.dart';

import 'package:movies_app/src/domain/domain_exports.dart';

class GetMovieDetailUseCase {
  final Repository _repository;

  GetMovieDetailUseCase(this._repository);

  Future<Either<String, MovieDetailEntity>> call(int movieId) async =>
      await _repository.getMovieDetail(movieId);
}
