import 'package:dartz/dartz.dart';

import 'package:movies_app/src/domain/domain_exports.dart'
    show Repository, MovieEntity;

class GetTopRatedMoviesUseCase {
  final Repository _repository;

  GetTopRatedMoviesUseCase(this._repository);

  Future<Either<String, List<MovieEntity>>> call() async =>
      await _repository.getTopRatedMovies();
}
