import 'package:equatable/equatable.dart';

class MovieDetailEntity extends Equatable {
  final int id;
  final String title;
  final String overview;
  final String posterPath;
  final double voteAverage;
  final String releaseDate;
  final String backdropPath;
  final String originalTitle;
  final List<GenreEntity> genres;
  final List<ProductionCompanyEntity> productionCompanies;

  const MovieDetailEntity({
    required this.id,
    required this.title,
    required this.genres,
    required this.overview,
    required this.posterPath,
    required this.voteAverage,
    required this.releaseDate,
    required this.backdropPath,
    required this.originalTitle,
    required this.productionCompanies,
  });

  @override
  List<Object?> get props => [
        id,
        title,
        genres,
        overview,
        posterPath,
        voteAverage,
        releaseDate,
        backdropPath,
        originalTitle,
        productionCompanies,
      ];
}

class GenreEntity extends Equatable {
  final int id;
  final String name;

  const GenreEntity({required this.id, required this.name});

  @override
  List<Object?> get props => [id, name];
}

class ProductionCompanyEntity extends Equatable {
  final int id;
  final String name;
  final String originCountry;

  const ProductionCompanyEntity({
    required this.id,
    required this.name,
    required this.originCountry,
  });

  @override
  List<Object?> get props => [id, name, originCountry];
}
