import 'package:equatable/equatable.dart';

class MovieActorEntity extends Equatable {
  final int id;
  final String name;
  final String character;
  final String profilePath;

  const MovieActorEntity({
    required this.id,
    required this.name,
    required this.character,
    required this.profilePath,
  });

  @override
  List<Object?> get props => [id, name, character, profilePath];
}
