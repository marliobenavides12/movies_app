import 'package:equatable/equatable.dart';

class MovieEntity extends Equatable {
  final int id;
  final String title;
  final String posterPath;
  final double voteAverage;
  final String backdropPath;
  final String originalTitle;

  const MovieEntity({
    required this.id,
    required this.title,
    required this.posterPath,
    required this.voteAverage,
    required this.backdropPath,
    required this.originalTitle,
  });

  @override
  List<Object?> get props => [
        id,
        title,
        posterPath,
        voteAverage,
        backdropPath,
        originalTitle,
      ];
}
