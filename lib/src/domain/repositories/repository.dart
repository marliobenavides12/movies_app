import 'package:dartz/dartz.dart';

import 'package:movies_app/src/domain/domain_exports.dart';

abstract class Repository {
  Future<Either<String, List<MovieEntity>>> getPopularMovies();
  Future<Either<String, List<MovieEntity>>> getTopRatedMovies();
  Future<Either<String, MovieDetailEntity>> getMovieDetail(int movieId);
  Future<Either<String, List<MovieActorEntity>>> getMovieActors(int movieId);
}
