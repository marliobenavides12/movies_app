import 'package:intl/intl.dart';
import 'package:movies_app/src/core/core_exports.dart' show Env, Constant;

class AppFunctions {
  AppFunctions._();

  static String validateImage(String? image) {
    if (image == null || image.isEmpty) {
      return Constant.noImg;
    } else {
      return Env.imageHost + image;
    }
  }

  static String formatDate(String dateText) =>
      DateFormat.y().format(DateTime.parse(dateText));
}
