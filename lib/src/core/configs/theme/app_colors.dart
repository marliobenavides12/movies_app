import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  static const Color darkBlue = Color(0xFF2b3848);
  static const Color lightBlue = Color(0xFF5ca0d3);
  static const Color primaryBlue = Color(0xFF4767EE);

  static const Color darkBlack = Color(0xFF1C1C1E);
  static const Color lightBlack = Color(0xFF2C2C2E);

  static const Color error = Color(0xFFE1665D);
  static const Color success = Color(0xFF28A746);
  static const Color warning = Color(0xFFfd7e14);

  static const Color yellow = Color(0xFFfbd303);
  static Color yellowOpacity = const Color(0xFFfbd303).withOpacity(0.4);
}
