import 'package:flutter/material.dart';

import 'package:movies_app/src/core/core_exports.dart' show AppColors;

class AppTheme {
  AppTheme._();

  static ThemeData darkTheme = ThemeData(
    useMaterial3: true,
    colorScheme: const ColorScheme.dark(
      background: Colors.white,
      primary: AppColors.lightBlack,
      secondary: AppColors.darkBlack,
    ),
  );

  static ThemeData lightTheme = ThemeData(
    useMaterial3: true,
    colorScheme: const ColorScheme.dark(
      background: Colors.white,
      primary: AppColors.lightBlue,
      secondary: AppColors.darkBlue,
    ),
  );
}
