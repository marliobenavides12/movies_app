import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import 'package:movies_app/src/presentation/pages/pages_exports.dart';

final appRouter = GoRouter(
  initialLocation: '/',
  routes: [
    GoRoute(
        path: '/',
        name: 'home',
        builder: (context, state) => const HomePage(),
        routes: [
          GoRoute(
            path: 'movieInfo/:id',
            name: 'movieInfo',
            pageBuilder: (context, state) {
              final int movieId = int.parse(state.pathParameters['id']!);
              // return MovieInfoPage(movieId: movieId);
              return CustomTransitionPage<void>(
                // opaque: false,
                key: state.pageKey,
                // barrierDismissible: true,
                barrierColor: Colors.black38,
                transitionDuration: const Duration(milliseconds: 600),
                child: MovieInfoPage(movieId: movieId),
                // transitionsBuilder: (_, __, ___, Widget child) => child,
                transitionsBuilder: (BuildContext context,
                    Animation<double> animation,
                    Animation<double> secondaryAnimation,
                    Widget child) {
                  //      return SlideTransition(
                  //   position: animation.drive(
                  //     Tween<Offset>(
                  //       begin: const Offset(0.0, 0.0),
                  //       end: Offset.zero,
                  //     ).chain(
                  //       CurveTween(curve: Curves.easeInOutCirc),
                  //     ),
                  //   ),
                  //   child: child,
                  // );
                  return FadeTransition(
                    opacity:
                        CurveTween(curve: Curves.easeInOut).animate(animation),
                    child: child,
                  );
                },
              );
              // return CustomTransitionPage(
              //   key: state.pageKey,
              //   child: DetailsScreen(),
              //   transitionsBuilder:
              //       (context, animation, secondaryAnimation, child) {
              //     // Change the opacity of the screen using a Curve based on the the animation's
              //     // value
              //     return FadeTransition(
              //       opacity: CurveTween(curve: Curves.easeInOutCirc)
              //           .animate(animation),
              //       child: child,
              //     );
              //   },
              // );
            },
          ),
        ]),
  ],
);
