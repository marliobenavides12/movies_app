enum TypeTheme { dark, light }

class Constant {
  Constant._();

  static const TypeTheme themeDark = TypeTheme.dark;
  static const TypeTheme themeLight = TypeTheme.light;
  static const String noImg =
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRh6n0Q3H_HR7BWc67gTgXwYHOcXBWGO8gKNtJBxnEkwxRgd-4LCdxPdPEsxHufQnFzEjQ&usqp=CAU';
}
