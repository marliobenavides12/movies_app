import 'package:flutter_dotenv/flutter_dotenv.dart';

class Env {
  Env._();

  static String accessToken = dotenv.get('ACCESS_TOKEN');

  static String movieApiUrl = dotenv.get('API_URL');
  static String moviePopularUrl = '${movieApiUrl}popular';
  static String movieTopRatedUrl = '${movieApiUrl}top_rated';
  static String imageHost = dotenv.get('IMAGE_HOST');

  static String paramLanguageEn = dotenv.get('PARAM_LANGUAGE_EN');
  static String paramLanguageEs = dotenv.get('PARAM_LANGUAGE_ES');
}
