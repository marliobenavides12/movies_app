part of 'home_bloc.dart';

@freezed
class HomeEvent with _$HomeEvent {
   const HomeEvent._();

  const factory HomeEvent.initialEvent() = _InitialEvent;

  const factory HomeEvent.loadingData() = _LoadingEvent;

  const factory HomeEvent.getDataEvent() = _GetDataEvent;

  const factory HomeEvent.errorEvent() = _ErrorEvent;

}