part of 'home_bloc.dart';

@freezed
class HomeState with _$HomeState {
  const HomeState._();

  const factory HomeState.initialState() = _InitialState;

  const factory HomeState.loadingState() = _LoadingState;

  const factory HomeState.dataLoadedState({
    required List<MovieEntity> popularMovies,
    required List<MovieEntity> topRatedMovies,
  }) = _DataLoadedState;

  const factory HomeState.noDataState() = _NoDataState;

  const factory HomeState.errorState({required String errorMesage}) =
      _ErrorState;
}
