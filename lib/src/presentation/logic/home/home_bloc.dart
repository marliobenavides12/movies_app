import 'package:bloc/bloc.dart';

import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:movies_app/src/domain/domain_exports.dart';

part 'home_event.dart';
part 'home_state.dart';
part 'home_bloc.freezed.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final GetPopularMoviesUseCase getPopularMoviesUseCase;
  final GetTopRatedMoviesUseCase getTopRatedMoviesUseCase;

  HomeBloc({
    required this.getPopularMoviesUseCase,
    required this.getTopRatedMoviesUseCase,
  }) : super(const _InitialState()) {
    on<HomeEvent>(_onGetNotes);
  }

  void _onGetNotes(HomeEvent event, Emitter<HomeState> emit) async {
    if (event is _LoadingEvent) {
      emit(const _LoadingState());
      String? errrorPopular;
      String? errrorTopRated;

      List<MovieEntity> dataPopular = [];
      List<MovieEntity> dataTopRated = [];

      final resultPopular = await getPopularMoviesUseCase.call();
      resultPopular.fold(
        (error) => errrorPopular = error,
        (List<MovieEntity> data) => dataPopular = data,
      );

      final resultTopRated = await getTopRatedMoviesUseCase.call();
      resultTopRated.fold(
        (error) => errrorTopRated = error,
        (List<MovieEntity> data) => dataTopRated = data,
      );

      if (errrorPopular != null || errrorTopRated != null) {
        emit(_ErrorState(errorMesage: '$errrorPopular $errrorTopRated'));
      } else {
        if (dataPopular.isEmpty && dataTopRated.isEmpty) {
          emit(const _NoDataState());
        } else {
          emit(_DataLoadedState(
            popularMovies: dataPopular,
            topRatedMovies: dataTopRated,
          ));
        }
      }
    }
  }
}
