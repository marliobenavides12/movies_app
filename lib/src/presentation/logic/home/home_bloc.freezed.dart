// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'home_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$HomeEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initialEvent,
    required TResult Function() loadingData,
    required TResult Function() getDataEvent,
    required TResult Function() errorEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initialEvent,
    TResult? Function()? loadingData,
    TResult? Function()? getDataEvent,
    TResult? Function()? errorEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initialEvent,
    TResult Function()? loadingData,
    TResult Function()? getDataEvent,
    TResult Function()? errorEvent,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitialEvent value) initialEvent,
    required TResult Function(_LoadingEvent value) loadingData,
    required TResult Function(_GetDataEvent value) getDataEvent,
    required TResult Function(_ErrorEvent value) errorEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitialEvent value)? initialEvent,
    TResult? Function(_LoadingEvent value)? loadingData,
    TResult? Function(_GetDataEvent value)? getDataEvent,
    TResult? Function(_ErrorEvent value)? errorEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitialEvent value)? initialEvent,
    TResult Function(_LoadingEvent value)? loadingData,
    TResult Function(_GetDataEvent value)? getDataEvent,
    TResult Function(_ErrorEvent value)? errorEvent,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HomeEventCopyWith<$Res> {
  factory $HomeEventCopyWith(HomeEvent value, $Res Function(HomeEvent) then) =
      _$HomeEventCopyWithImpl<$Res, HomeEvent>;
}

/// @nodoc
class _$HomeEventCopyWithImpl<$Res, $Val extends HomeEvent>
    implements $HomeEventCopyWith<$Res> {
  _$HomeEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitialEventImplCopyWith<$Res> {
  factory _$$InitialEventImplCopyWith(
          _$InitialEventImpl value, $Res Function(_$InitialEventImpl) then) =
      __$$InitialEventImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialEventImplCopyWithImpl<$Res>
    extends _$HomeEventCopyWithImpl<$Res, _$InitialEventImpl>
    implements _$$InitialEventImplCopyWith<$Res> {
  __$$InitialEventImplCopyWithImpl(
      _$InitialEventImpl _value, $Res Function(_$InitialEventImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitialEventImpl extends _InitialEvent {
  const _$InitialEventImpl() : super._();

  @override
  String toString() {
    return 'HomeEvent.initialEvent()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InitialEventImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initialEvent,
    required TResult Function() loadingData,
    required TResult Function() getDataEvent,
    required TResult Function() errorEvent,
  }) {
    return initialEvent();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initialEvent,
    TResult? Function()? loadingData,
    TResult? Function()? getDataEvent,
    TResult? Function()? errorEvent,
  }) {
    return initialEvent?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initialEvent,
    TResult Function()? loadingData,
    TResult Function()? getDataEvent,
    TResult Function()? errorEvent,
    required TResult orElse(),
  }) {
    if (initialEvent != null) {
      return initialEvent();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitialEvent value) initialEvent,
    required TResult Function(_LoadingEvent value) loadingData,
    required TResult Function(_GetDataEvent value) getDataEvent,
    required TResult Function(_ErrorEvent value) errorEvent,
  }) {
    return initialEvent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitialEvent value)? initialEvent,
    TResult? Function(_LoadingEvent value)? loadingData,
    TResult? Function(_GetDataEvent value)? getDataEvent,
    TResult? Function(_ErrorEvent value)? errorEvent,
  }) {
    return initialEvent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitialEvent value)? initialEvent,
    TResult Function(_LoadingEvent value)? loadingData,
    TResult Function(_GetDataEvent value)? getDataEvent,
    TResult Function(_ErrorEvent value)? errorEvent,
    required TResult orElse(),
  }) {
    if (initialEvent != null) {
      return initialEvent(this);
    }
    return orElse();
  }
}

abstract class _InitialEvent extends HomeEvent {
  const factory _InitialEvent() = _$InitialEventImpl;
  const _InitialEvent._() : super._();
}

/// @nodoc
abstract class _$$LoadingEventImplCopyWith<$Res> {
  factory _$$LoadingEventImplCopyWith(
          _$LoadingEventImpl value, $Res Function(_$LoadingEventImpl) then) =
      __$$LoadingEventImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadingEventImplCopyWithImpl<$Res>
    extends _$HomeEventCopyWithImpl<$Res, _$LoadingEventImpl>
    implements _$$LoadingEventImplCopyWith<$Res> {
  __$$LoadingEventImplCopyWithImpl(
      _$LoadingEventImpl _value, $Res Function(_$LoadingEventImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$LoadingEventImpl extends _LoadingEvent {
  const _$LoadingEventImpl() : super._();

  @override
  String toString() {
    return 'HomeEvent.loadingData()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$LoadingEventImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initialEvent,
    required TResult Function() loadingData,
    required TResult Function() getDataEvent,
    required TResult Function() errorEvent,
  }) {
    return loadingData();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initialEvent,
    TResult? Function()? loadingData,
    TResult? Function()? getDataEvent,
    TResult? Function()? errorEvent,
  }) {
    return loadingData?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initialEvent,
    TResult Function()? loadingData,
    TResult Function()? getDataEvent,
    TResult Function()? errorEvent,
    required TResult orElse(),
  }) {
    if (loadingData != null) {
      return loadingData();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitialEvent value) initialEvent,
    required TResult Function(_LoadingEvent value) loadingData,
    required TResult Function(_GetDataEvent value) getDataEvent,
    required TResult Function(_ErrorEvent value) errorEvent,
  }) {
    return loadingData(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitialEvent value)? initialEvent,
    TResult? Function(_LoadingEvent value)? loadingData,
    TResult? Function(_GetDataEvent value)? getDataEvent,
    TResult? Function(_ErrorEvent value)? errorEvent,
  }) {
    return loadingData?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitialEvent value)? initialEvent,
    TResult Function(_LoadingEvent value)? loadingData,
    TResult Function(_GetDataEvent value)? getDataEvent,
    TResult Function(_ErrorEvent value)? errorEvent,
    required TResult orElse(),
  }) {
    if (loadingData != null) {
      return loadingData(this);
    }
    return orElse();
  }
}

abstract class _LoadingEvent extends HomeEvent {
  const factory _LoadingEvent() = _$LoadingEventImpl;
  const _LoadingEvent._() : super._();
}

/// @nodoc
abstract class _$$GetDataEventImplCopyWith<$Res> {
  factory _$$GetDataEventImplCopyWith(
          _$GetDataEventImpl value, $Res Function(_$GetDataEventImpl) then) =
      __$$GetDataEventImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GetDataEventImplCopyWithImpl<$Res>
    extends _$HomeEventCopyWithImpl<$Res, _$GetDataEventImpl>
    implements _$$GetDataEventImplCopyWith<$Res> {
  __$$GetDataEventImplCopyWithImpl(
      _$GetDataEventImpl _value, $Res Function(_$GetDataEventImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GetDataEventImpl extends _GetDataEvent {
  const _$GetDataEventImpl() : super._();

  @override
  String toString() {
    return 'HomeEvent.getDataEvent()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GetDataEventImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initialEvent,
    required TResult Function() loadingData,
    required TResult Function() getDataEvent,
    required TResult Function() errorEvent,
  }) {
    return getDataEvent();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initialEvent,
    TResult? Function()? loadingData,
    TResult? Function()? getDataEvent,
    TResult? Function()? errorEvent,
  }) {
    return getDataEvent?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initialEvent,
    TResult Function()? loadingData,
    TResult Function()? getDataEvent,
    TResult Function()? errorEvent,
    required TResult orElse(),
  }) {
    if (getDataEvent != null) {
      return getDataEvent();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitialEvent value) initialEvent,
    required TResult Function(_LoadingEvent value) loadingData,
    required TResult Function(_GetDataEvent value) getDataEvent,
    required TResult Function(_ErrorEvent value) errorEvent,
  }) {
    return getDataEvent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitialEvent value)? initialEvent,
    TResult? Function(_LoadingEvent value)? loadingData,
    TResult? Function(_GetDataEvent value)? getDataEvent,
    TResult? Function(_ErrorEvent value)? errorEvent,
  }) {
    return getDataEvent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitialEvent value)? initialEvent,
    TResult Function(_LoadingEvent value)? loadingData,
    TResult Function(_GetDataEvent value)? getDataEvent,
    TResult Function(_ErrorEvent value)? errorEvent,
    required TResult orElse(),
  }) {
    if (getDataEvent != null) {
      return getDataEvent(this);
    }
    return orElse();
  }
}

abstract class _GetDataEvent extends HomeEvent {
  const factory _GetDataEvent() = _$GetDataEventImpl;
  const _GetDataEvent._() : super._();
}

/// @nodoc
abstract class _$$ErrorEventImplCopyWith<$Res> {
  factory _$$ErrorEventImplCopyWith(
          _$ErrorEventImpl value, $Res Function(_$ErrorEventImpl) then) =
      __$$ErrorEventImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ErrorEventImplCopyWithImpl<$Res>
    extends _$HomeEventCopyWithImpl<$Res, _$ErrorEventImpl>
    implements _$$ErrorEventImplCopyWith<$Res> {
  __$$ErrorEventImplCopyWithImpl(
      _$ErrorEventImpl _value, $Res Function(_$ErrorEventImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ErrorEventImpl extends _ErrorEvent {
  const _$ErrorEventImpl() : super._();

  @override
  String toString() {
    return 'HomeEvent.errorEvent()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ErrorEventImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initialEvent,
    required TResult Function() loadingData,
    required TResult Function() getDataEvent,
    required TResult Function() errorEvent,
  }) {
    return errorEvent();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initialEvent,
    TResult? Function()? loadingData,
    TResult? Function()? getDataEvent,
    TResult? Function()? errorEvent,
  }) {
    return errorEvent?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initialEvent,
    TResult Function()? loadingData,
    TResult Function()? getDataEvent,
    TResult Function()? errorEvent,
    required TResult orElse(),
  }) {
    if (errorEvent != null) {
      return errorEvent();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitialEvent value) initialEvent,
    required TResult Function(_LoadingEvent value) loadingData,
    required TResult Function(_GetDataEvent value) getDataEvent,
    required TResult Function(_ErrorEvent value) errorEvent,
  }) {
    return errorEvent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitialEvent value)? initialEvent,
    TResult? Function(_LoadingEvent value)? loadingData,
    TResult? Function(_GetDataEvent value)? getDataEvent,
    TResult? Function(_ErrorEvent value)? errorEvent,
  }) {
    return errorEvent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitialEvent value)? initialEvent,
    TResult Function(_LoadingEvent value)? loadingData,
    TResult Function(_GetDataEvent value)? getDataEvent,
    TResult Function(_ErrorEvent value)? errorEvent,
    required TResult orElse(),
  }) {
    if (errorEvent != null) {
      return errorEvent(this);
    }
    return orElse();
  }
}

abstract class _ErrorEvent extends HomeEvent {
  const factory _ErrorEvent() = _$ErrorEventImpl;
  const _ErrorEvent._() : super._();
}

/// @nodoc
mixin _$HomeState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initialState,
    required TResult Function() loadingState,
    required TResult Function(
            List<MovieEntity> popularMovies, List<MovieEntity> topRatedMovies)
        dataLoadedState,
    required TResult Function() noDataState,
    required TResult Function(String errorMesage) errorState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initialState,
    TResult? Function()? loadingState,
    TResult? Function(
            List<MovieEntity> popularMovies, List<MovieEntity> topRatedMovies)?
        dataLoadedState,
    TResult? Function()? noDataState,
    TResult? Function(String errorMesage)? errorState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function()? loadingState,
    TResult Function(
            List<MovieEntity> popularMovies, List<MovieEntity> topRatedMovies)?
        dataLoadedState,
    TResult Function()? noDataState,
    TResult Function(String errorMesage)? errorState,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitialState value) initialState,
    required TResult Function(_LoadingState value) loadingState,
    required TResult Function(_DataLoadedState value) dataLoadedState,
    required TResult Function(_NoDataState value) noDataState,
    required TResult Function(_ErrorState value) errorState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitialState value)? initialState,
    TResult? Function(_LoadingState value)? loadingState,
    TResult? Function(_DataLoadedState value)? dataLoadedState,
    TResult? Function(_NoDataState value)? noDataState,
    TResult? Function(_ErrorState value)? errorState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitialState value)? initialState,
    TResult Function(_LoadingState value)? loadingState,
    TResult Function(_DataLoadedState value)? dataLoadedState,
    TResult Function(_NoDataState value)? noDataState,
    TResult Function(_ErrorState value)? errorState,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HomeStateCopyWith<$Res> {
  factory $HomeStateCopyWith(HomeState value, $Res Function(HomeState) then) =
      _$HomeStateCopyWithImpl<$Res, HomeState>;
}

/// @nodoc
class _$HomeStateCopyWithImpl<$Res, $Val extends HomeState>
    implements $HomeStateCopyWith<$Res> {
  _$HomeStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitialStateImplCopyWith<$Res> {
  factory _$$InitialStateImplCopyWith(
          _$InitialStateImpl value, $Res Function(_$InitialStateImpl) then) =
      __$$InitialStateImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialStateImplCopyWithImpl<$Res>
    extends _$HomeStateCopyWithImpl<$Res, _$InitialStateImpl>
    implements _$$InitialStateImplCopyWith<$Res> {
  __$$InitialStateImplCopyWithImpl(
      _$InitialStateImpl _value, $Res Function(_$InitialStateImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitialStateImpl extends _InitialState {
  const _$InitialStateImpl() : super._();

  @override
  String toString() {
    return 'HomeState.initialState()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InitialStateImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initialState,
    required TResult Function() loadingState,
    required TResult Function(
            List<MovieEntity> popularMovies, List<MovieEntity> topRatedMovies)
        dataLoadedState,
    required TResult Function() noDataState,
    required TResult Function(String errorMesage) errorState,
  }) {
    return initialState();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initialState,
    TResult? Function()? loadingState,
    TResult? Function(
            List<MovieEntity> popularMovies, List<MovieEntity> topRatedMovies)?
        dataLoadedState,
    TResult? Function()? noDataState,
    TResult? Function(String errorMesage)? errorState,
  }) {
    return initialState?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function()? loadingState,
    TResult Function(
            List<MovieEntity> popularMovies, List<MovieEntity> topRatedMovies)?
        dataLoadedState,
    TResult Function()? noDataState,
    TResult Function(String errorMesage)? errorState,
    required TResult orElse(),
  }) {
    if (initialState != null) {
      return initialState();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitialState value) initialState,
    required TResult Function(_LoadingState value) loadingState,
    required TResult Function(_DataLoadedState value) dataLoadedState,
    required TResult Function(_NoDataState value) noDataState,
    required TResult Function(_ErrorState value) errorState,
  }) {
    return initialState(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitialState value)? initialState,
    TResult? Function(_LoadingState value)? loadingState,
    TResult? Function(_DataLoadedState value)? dataLoadedState,
    TResult? Function(_NoDataState value)? noDataState,
    TResult? Function(_ErrorState value)? errorState,
  }) {
    return initialState?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitialState value)? initialState,
    TResult Function(_LoadingState value)? loadingState,
    TResult Function(_DataLoadedState value)? dataLoadedState,
    TResult Function(_NoDataState value)? noDataState,
    TResult Function(_ErrorState value)? errorState,
    required TResult orElse(),
  }) {
    if (initialState != null) {
      return initialState(this);
    }
    return orElse();
  }
}

abstract class _InitialState extends HomeState {
  const factory _InitialState() = _$InitialStateImpl;
  const _InitialState._() : super._();
}

/// @nodoc
abstract class _$$LoadingStateImplCopyWith<$Res> {
  factory _$$LoadingStateImplCopyWith(
          _$LoadingStateImpl value, $Res Function(_$LoadingStateImpl) then) =
      __$$LoadingStateImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadingStateImplCopyWithImpl<$Res>
    extends _$HomeStateCopyWithImpl<$Res, _$LoadingStateImpl>
    implements _$$LoadingStateImplCopyWith<$Res> {
  __$$LoadingStateImplCopyWithImpl(
      _$LoadingStateImpl _value, $Res Function(_$LoadingStateImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$LoadingStateImpl extends _LoadingState {
  const _$LoadingStateImpl() : super._();

  @override
  String toString() {
    return 'HomeState.loadingState()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$LoadingStateImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initialState,
    required TResult Function() loadingState,
    required TResult Function(
            List<MovieEntity> popularMovies, List<MovieEntity> topRatedMovies)
        dataLoadedState,
    required TResult Function() noDataState,
    required TResult Function(String errorMesage) errorState,
  }) {
    return loadingState();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initialState,
    TResult? Function()? loadingState,
    TResult? Function(
            List<MovieEntity> popularMovies, List<MovieEntity> topRatedMovies)?
        dataLoadedState,
    TResult? Function()? noDataState,
    TResult? Function(String errorMesage)? errorState,
  }) {
    return loadingState?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function()? loadingState,
    TResult Function(
            List<MovieEntity> popularMovies, List<MovieEntity> topRatedMovies)?
        dataLoadedState,
    TResult Function()? noDataState,
    TResult Function(String errorMesage)? errorState,
    required TResult orElse(),
  }) {
    if (loadingState != null) {
      return loadingState();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitialState value) initialState,
    required TResult Function(_LoadingState value) loadingState,
    required TResult Function(_DataLoadedState value) dataLoadedState,
    required TResult Function(_NoDataState value) noDataState,
    required TResult Function(_ErrorState value) errorState,
  }) {
    return loadingState(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitialState value)? initialState,
    TResult? Function(_LoadingState value)? loadingState,
    TResult? Function(_DataLoadedState value)? dataLoadedState,
    TResult? Function(_NoDataState value)? noDataState,
    TResult? Function(_ErrorState value)? errorState,
  }) {
    return loadingState?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitialState value)? initialState,
    TResult Function(_LoadingState value)? loadingState,
    TResult Function(_DataLoadedState value)? dataLoadedState,
    TResult Function(_NoDataState value)? noDataState,
    TResult Function(_ErrorState value)? errorState,
    required TResult orElse(),
  }) {
    if (loadingState != null) {
      return loadingState(this);
    }
    return orElse();
  }
}

abstract class _LoadingState extends HomeState {
  const factory _LoadingState() = _$LoadingStateImpl;
  const _LoadingState._() : super._();
}

/// @nodoc
abstract class _$$DataLoadedStateImplCopyWith<$Res> {
  factory _$$DataLoadedStateImplCopyWith(_$DataLoadedStateImpl value,
          $Res Function(_$DataLoadedStateImpl) then) =
      __$$DataLoadedStateImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {List<MovieEntity> popularMovies, List<MovieEntity> topRatedMovies});
}

/// @nodoc
class __$$DataLoadedStateImplCopyWithImpl<$Res>
    extends _$HomeStateCopyWithImpl<$Res, _$DataLoadedStateImpl>
    implements _$$DataLoadedStateImplCopyWith<$Res> {
  __$$DataLoadedStateImplCopyWithImpl(
      _$DataLoadedStateImpl _value, $Res Function(_$DataLoadedStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? popularMovies = null,
    Object? topRatedMovies = null,
  }) {
    return _then(_$DataLoadedStateImpl(
      popularMovies: null == popularMovies
          ? _value._popularMovies
          : popularMovies // ignore: cast_nullable_to_non_nullable
              as List<MovieEntity>,
      topRatedMovies: null == topRatedMovies
          ? _value._topRatedMovies
          : topRatedMovies // ignore: cast_nullable_to_non_nullable
              as List<MovieEntity>,
    ));
  }
}

/// @nodoc

class _$DataLoadedStateImpl extends _DataLoadedState {
  const _$DataLoadedStateImpl(
      {required final List<MovieEntity> popularMovies,
      required final List<MovieEntity> topRatedMovies})
      : _popularMovies = popularMovies,
        _topRatedMovies = topRatedMovies,
        super._();

  final List<MovieEntity> _popularMovies;
  @override
  List<MovieEntity> get popularMovies {
    if (_popularMovies is EqualUnmodifiableListView) return _popularMovies;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_popularMovies);
  }

  final List<MovieEntity> _topRatedMovies;
  @override
  List<MovieEntity> get topRatedMovies {
    if (_topRatedMovies is EqualUnmodifiableListView) return _topRatedMovies;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_topRatedMovies);
  }

  @override
  String toString() {
    return 'HomeState.dataLoadedState(popularMovies: $popularMovies, topRatedMovies: $topRatedMovies)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DataLoadedStateImpl &&
            const DeepCollectionEquality()
                .equals(other._popularMovies, _popularMovies) &&
            const DeepCollectionEquality()
                .equals(other._topRatedMovies, _topRatedMovies));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_popularMovies),
      const DeepCollectionEquality().hash(_topRatedMovies));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DataLoadedStateImplCopyWith<_$DataLoadedStateImpl> get copyWith =>
      __$$DataLoadedStateImplCopyWithImpl<_$DataLoadedStateImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initialState,
    required TResult Function() loadingState,
    required TResult Function(
            List<MovieEntity> popularMovies, List<MovieEntity> topRatedMovies)
        dataLoadedState,
    required TResult Function() noDataState,
    required TResult Function(String errorMesage) errorState,
  }) {
    return dataLoadedState(popularMovies, topRatedMovies);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initialState,
    TResult? Function()? loadingState,
    TResult? Function(
            List<MovieEntity> popularMovies, List<MovieEntity> topRatedMovies)?
        dataLoadedState,
    TResult? Function()? noDataState,
    TResult? Function(String errorMesage)? errorState,
  }) {
    return dataLoadedState?.call(popularMovies, topRatedMovies);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function()? loadingState,
    TResult Function(
            List<MovieEntity> popularMovies, List<MovieEntity> topRatedMovies)?
        dataLoadedState,
    TResult Function()? noDataState,
    TResult Function(String errorMesage)? errorState,
    required TResult orElse(),
  }) {
    if (dataLoadedState != null) {
      return dataLoadedState(popularMovies, topRatedMovies);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitialState value) initialState,
    required TResult Function(_LoadingState value) loadingState,
    required TResult Function(_DataLoadedState value) dataLoadedState,
    required TResult Function(_NoDataState value) noDataState,
    required TResult Function(_ErrorState value) errorState,
  }) {
    return dataLoadedState(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitialState value)? initialState,
    TResult? Function(_LoadingState value)? loadingState,
    TResult? Function(_DataLoadedState value)? dataLoadedState,
    TResult? Function(_NoDataState value)? noDataState,
    TResult? Function(_ErrorState value)? errorState,
  }) {
    return dataLoadedState?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitialState value)? initialState,
    TResult Function(_LoadingState value)? loadingState,
    TResult Function(_DataLoadedState value)? dataLoadedState,
    TResult Function(_NoDataState value)? noDataState,
    TResult Function(_ErrorState value)? errorState,
    required TResult orElse(),
  }) {
    if (dataLoadedState != null) {
      return dataLoadedState(this);
    }
    return orElse();
  }
}

abstract class _DataLoadedState extends HomeState {
  const factory _DataLoadedState(
      {required final List<MovieEntity> popularMovies,
      required final List<MovieEntity> topRatedMovies}) = _$DataLoadedStateImpl;
  const _DataLoadedState._() : super._();

  List<MovieEntity> get popularMovies;
  List<MovieEntity> get topRatedMovies;
  @JsonKey(ignore: true)
  _$$DataLoadedStateImplCopyWith<_$DataLoadedStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$NoDataStateImplCopyWith<$Res> {
  factory _$$NoDataStateImplCopyWith(
          _$NoDataStateImpl value, $Res Function(_$NoDataStateImpl) then) =
      __$$NoDataStateImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$NoDataStateImplCopyWithImpl<$Res>
    extends _$HomeStateCopyWithImpl<$Res, _$NoDataStateImpl>
    implements _$$NoDataStateImplCopyWith<$Res> {
  __$$NoDataStateImplCopyWithImpl(
      _$NoDataStateImpl _value, $Res Function(_$NoDataStateImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$NoDataStateImpl extends _NoDataState {
  const _$NoDataStateImpl() : super._();

  @override
  String toString() {
    return 'HomeState.noDataState()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$NoDataStateImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initialState,
    required TResult Function() loadingState,
    required TResult Function(
            List<MovieEntity> popularMovies, List<MovieEntity> topRatedMovies)
        dataLoadedState,
    required TResult Function() noDataState,
    required TResult Function(String errorMesage) errorState,
  }) {
    return noDataState();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initialState,
    TResult? Function()? loadingState,
    TResult? Function(
            List<MovieEntity> popularMovies, List<MovieEntity> topRatedMovies)?
        dataLoadedState,
    TResult? Function()? noDataState,
    TResult? Function(String errorMesage)? errorState,
  }) {
    return noDataState?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function()? loadingState,
    TResult Function(
            List<MovieEntity> popularMovies, List<MovieEntity> topRatedMovies)?
        dataLoadedState,
    TResult Function()? noDataState,
    TResult Function(String errorMesage)? errorState,
    required TResult orElse(),
  }) {
    if (noDataState != null) {
      return noDataState();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitialState value) initialState,
    required TResult Function(_LoadingState value) loadingState,
    required TResult Function(_DataLoadedState value) dataLoadedState,
    required TResult Function(_NoDataState value) noDataState,
    required TResult Function(_ErrorState value) errorState,
  }) {
    return noDataState(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitialState value)? initialState,
    TResult? Function(_LoadingState value)? loadingState,
    TResult? Function(_DataLoadedState value)? dataLoadedState,
    TResult? Function(_NoDataState value)? noDataState,
    TResult? Function(_ErrorState value)? errorState,
  }) {
    return noDataState?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitialState value)? initialState,
    TResult Function(_LoadingState value)? loadingState,
    TResult Function(_DataLoadedState value)? dataLoadedState,
    TResult Function(_NoDataState value)? noDataState,
    TResult Function(_ErrorState value)? errorState,
    required TResult orElse(),
  }) {
    if (noDataState != null) {
      return noDataState(this);
    }
    return orElse();
  }
}

abstract class _NoDataState extends HomeState {
  const factory _NoDataState() = _$NoDataStateImpl;
  const _NoDataState._() : super._();
}

/// @nodoc
abstract class _$$ErrorStateImplCopyWith<$Res> {
  factory _$$ErrorStateImplCopyWith(
          _$ErrorStateImpl value, $Res Function(_$ErrorStateImpl) then) =
      __$$ErrorStateImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String errorMesage});
}

/// @nodoc
class __$$ErrorStateImplCopyWithImpl<$Res>
    extends _$HomeStateCopyWithImpl<$Res, _$ErrorStateImpl>
    implements _$$ErrorStateImplCopyWith<$Res> {
  __$$ErrorStateImplCopyWithImpl(
      _$ErrorStateImpl _value, $Res Function(_$ErrorStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? errorMesage = null,
  }) {
    return _then(_$ErrorStateImpl(
      errorMesage: null == errorMesage
          ? _value.errorMesage
          : errorMesage // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ErrorStateImpl extends _ErrorState {
  const _$ErrorStateImpl({required this.errorMesage}) : super._();

  @override
  final String errorMesage;

  @override
  String toString() {
    return 'HomeState.errorState(errorMesage: $errorMesage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ErrorStateImpl &&
            (identical(other.errorMesage, errorMesage) ||
                other.errorMesage == errorMesage));
  }

  @override
  int get hashCode => Object.hash(runtimeType, errorMesage);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ErrorStateImplCopyWith<_$ErrorStateImpl> get copyWith =>
      __$$ErrorStateImplCopyWithImpl<_$ErrorStateImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initialState,
    required TResult Function() loadingState,
    required TResult Function(
            List<MovieEntity> popularMovies, List<MovieEntity> topRatedMovies)
        dataLoadedState,
    required TResult Function() noDataState,
    required TResult Function(String errorMesage) errorState,
  }) {
    return errorState(errorMesage);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initialState,
    TResult? Function()? loadingState,
    TResult? Function(
            List<MovieEntity> popularMovies, List<MovieEntity> topRatedMovies)?
        dataLoadedState,
    TResult? Function()? noDataState,
    TResult? Function(String errorMesage)? errorState,
  }) {
    return errorState?.call(errorMesage);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function()? loadingState,
    TResult Function(
            List<MovieEntity> popularMovies, List<MovieEntity> topRatedMovies)?
        dataLoadedState,
    TResult Function()? noDataState,
    TResult Function(String errorMesage)? errorState,
    required TResult orElse(),
  }) {
    if (errorState != null) {
      return errorState(errorMesage);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitialState value) initialState,
    required TResult Function(_LoadingState value) loadingState,
    required TResult Function(_DataLoadedState value) dataLoadedState,
    required TResult Function(_NoDataState value) noDataState,
    required TResult Function(_ErrorState value) errorState,
  }) {
    return errorState(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitialState value)? initialState,
    TResult? Function(_LoadingState value)? loadingState,
    TResult? Function(_DataLoadedState value)? dataLoadedState,
    TResult? Function(_NoDataState value)? noDataState,
    TResult? Function(_ErrorState value)? errorState,
  }) {
    return errorState?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitialState value)? initialState,
    TResult Function(_LoadingState value)? loadingState,
    TResult Function(_DataLoadedState value)? dataLoadedState,
    TResult Function(_NoDataState value)? noDataState,
    TResult Function(_ErrorState value)? errorState,
    required TResult orElse(),
  }) {
    if (errorState != null) {
      return errorState(this);
    }
    return orElse();
  }
}

abstract class _ErrorState extends HomeState {
  const factory _ErrorState({required final String errorMesage}) =
      _$ErrorStateImpl;
  const _ErrorState._() : super._();

  String get errorMesage;
  @JsonKey(ignore: true)
  _$$ErrorStateImplCopyWith<_$ErrorStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
