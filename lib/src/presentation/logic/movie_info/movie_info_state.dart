part of 'movie_info_bloc.dart';

@freezed
class MovieInfoState with _$MovieInfoState {
  const MovieInfoState._();

  const factory MovieInfoState.initialState() = _InitialState;

  const factory MovieInfoState.loadingState() = _LoadingState;

  const factory MovieInfoState.dataLoadedState({
    required MovieDetailEntity movieDetail,
    required List<MovieActorEntity> movieActors,
  }) = _DataLoadedState;

  const factory MovieInfoState.noDataState() = _NoDataState;

  const factory MovieInfoState.errorState({required String errorMesage}) =
      _ErrorState;
}
