import 'package:bloc/bloc.dart';

import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:movies_app/src/domain/domain_exports.dart';

part 'movie_info_event.dart';
part 'movie_info_state.dart';
part 'movie_info_bloc.freezed.dart';

class MovieInfoBloc extends Bloc<MovieInfoEvent, MovieInfoState> {
  final GetMovieDetailUseCase getMovieDetailUseCase;
  final GetMovieActorsUseCase getMovieActorsUseCase;

  MovieInfoBloc({
    required this.getMovieDetailUseCase,
    required this.getMovieActorsUseCase,
  }) : super(const _InitialState()) {
    on<MovieInfoEvent>(_onGetNotes);
  }

  void _onGetNotes(MovieInfoEvent event, Emitter<MovieInfoState> emit) async {
    if (event is _LoadingEvent) {
      String? errrorMovieDetail;
      String? errrorMovieActors;
      late MovieDetailEntity dataMovieDetail;
      List<MovieActorEntity> dataMovieActor = [];

      final resultMovieDetail = await getMovieDetailUseCase.call(event.movieId);
      resultMovieDetail.fold(
        (error) => errrorMovieDetail = error,
        (MovieDetailEntity data) => dataMovieDetail = data,
      );

      final resultMovieActors = await getMovieActorsUseCase.call(event.movieId);
      resultMovieActors.fold(
        (error) => errrorMovieActors = error,
        (List<MovieActorEntity> data) => dataMovieActor = data,
      );

      if (errrorMovieDetail != null || errrorMovieActors != null) {
        emit(_ErrorState(errorMesage: '$errrorMovieDetail $errrorMovieActors'));
      } else {
        if (dataMovieActor.isEmpty) {
          emit(const _NoDataState());
        } else {
          emit(_DataLoadedState(
            movieActors: dataMovieActor,
            movieDetail: dataMovieDetail,
          ));
        }
      }
    }

    if (event is _InitialEvent) {
      emit(const _InitialState());
    }
  }
}
