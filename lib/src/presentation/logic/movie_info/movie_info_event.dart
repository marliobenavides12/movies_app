part of 'movie_info_bloc.dart';

@freezed
class MovieInfoEvent with _$MovieInfoEvent {
  const MovieInfoEvent._();

  const factory MovieInfoEvent.initialEvent() = _InitialEvent;

  const factory MovieInfoEvent.loadingData(int movieId) = _LoadingEvent;

  const factory MovieInfoEvent.getDataEvent() = _GetDataEvent;

  const factory MovieInfoEvent.errorEvent() = _ErrorEvent;
}
