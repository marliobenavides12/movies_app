part of 'change_theme_bloc.dart';

@freezed
class ChangeThemeState with _$ChangeThemeState {
  // const factory ChangeThemeState.initial() = _Initial;
  const factory ChangeThemeState.changeState(TypeTheme typeTheme) =
      _ChangeState;
}
