part of 'change_theme_bloc.dart';

@freezed
class ChangeThemeEvent with _$ChangeThemeEvent {
  // const factory ChangeThemeEvent.started() = _Started;
  const factory ChangeThemeEvent.changeEvent(TypeTheme typeTheme) =
      _ChangeEvent;
}
