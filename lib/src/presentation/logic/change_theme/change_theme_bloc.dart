import 'package:bloc/bloc.dart';

import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:movies_app/src/core/core_exports.dart';

part 'change_theme_event.dart';
part 'change_theme_state.dart';
part 'change_theme_bloc.freezed.dart';

class ChangeThemeBloc extends Bloc<ChangeThemeEvent, ChangeThemeState> {
  ChangeThemeBloc() : super(const _ChangeState(Constant.themeLight)) {
    on<ChangeThemeEvent>((event, emit) {
      if (event.copyWith().typeTheme == TypeTheme.dark) {
        emit(const _ChangeState(TypeTheme.light));
      }

      if (event.copyWith().typeTheme == TypeTheme.light) {
        emit(const _ChangeState(TypeTheme.dark));
      }
    });
  }
}
