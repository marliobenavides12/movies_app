// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'change_theme_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ChangeThemeEvent {
  TypeTheme get typeTheme => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(TypeTheme typeTheme) changeEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(TypeTheme typeTheme)? changeEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(TypeTheme typeTheme)? changeEvent,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ChangeEvent value) changeEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ChangeEvent value)? changeEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ChangeEvent value)? changeEvent,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ChangeThemeEventCopyWith<ChangeThemeEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChangeThemeEventCopyWith<$Res> {
  factory $ChangeThemeEventCopyWith(
          ChangeThemeEvent value, $Res Function(ChangeThemeEvent) then) =
      _$ChangeThemeEventCopyWithImpl<$Res, ChangeThemeEvent>;
  @useResult
  $Res call({TypeTheme typeTheme});
}

/// @nodoc
class _$ChangeThemeEventCopyWithImpl<$Res, $Val extends ChangeThemeEvent>
    implements $ChangeThemeEventCopyWith<$Res> {
  _$ChangeThemeEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? typeTheme = freezed,
  }) {
    return _then(_value.copyWith(
      typeTheme: freezed == typeTheme
          ? _value.typeTheme
          : typeTheme // ignore: cast_nullable_to_non_nullable
              as TypeTheme,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ChangeEventImplCopyWith<$Res>
    implements $ChangeThemeEventCopyWith<$Res> {
  factory _$$ChangeEventImplCopyWith(
          _$ChangeEventImpl value, $Res Function(_$ChangeEventImpl) then) =
      __$$ChangeEventImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({TypeTheme typeTheme});
}

/// @nodoc
class __$$ChangeEventImplCopyWithImpl<$Res>
    extends _$ChangeThemeEventCopyWithImpl<$Res, _$ChangeEventImpl>
    implements _$$ChangeEventImplCopyWith<$Res> {
  __$$ChangeEventImplCopyWithImpl(
      _$ChangeEventImpl _value, $Res Function(_$ChangeEventImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? typeTheme = freezed,
  }) {
    return _then(_$ChangeEventImpl(
      freezed == typeTheme
          ? _value.typeTheme
          : typeTheme // ignore: cast_nullable_to_non_nullable
              as TypeTheme,
    ));
  }
}

/// @nodoc

class _$ChangeEventImpl implements _ChangeEvent {
  const _$ChangeEventImpl(this.typeTheme);

  @override
  final TypeTheme typeTheme;

  @override
  String toString() {
    return 'ChangeThemeEvent.changeEvent(typeTheme: $typeTheme)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ChangeEventImpl &&
            const DeepCollectionEquality().equals(other.typeTheme, typeTheme));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(typeTheme));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ChangeEventImplCopyWith<_$ChangeEventImpl> get copyWith =>
      __$$ChangeEventImplCopyWithImpl<_$ChangeEventImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(TypeTheme typeTheme) changeEvent,
  }) {
    return changeEvent(typeTheme);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(TypeTheme typeTheme)? changeEvent,
  }) {
    return changeEvent?.call(typeTheme);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(TypeTheme typeTheme)? changeEvent,
    required TResult orElse(),
  }) {
    if (changeEvent != null) {
      return changeEvent(typeTheme);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ChangeEvent value) changeEvent,
  }) {
    return changeEvent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ChangeEvent value)? changeEvent,
  }) {
    return changeEvent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ChangeEvent value)? changeEvent,
    required TResult orElse(),
  }) {
    if (changeEvent != null) {
      return changeEvent(this);
    }
    return orElse();
  }
}

abstract class _ChangeEvent implements ChangeThemeEvent {
  const factory _ChangeEvent(final TypeTheme typeTheme) = _$ChangeEventImpl;

  @override
  TypeTheme get typeTheme;
  @override
  @JsonKey(ignore: true)
  _$$ChangeEventImplCopyWith<_$ChangeEventImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$ChangeThemeState {
  TypeTheme get typeTheme => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(TypeTheme typeTheme) changeState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(TypeTheme typeTheme)? changeState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(TypeTheme typeTheme)? changeState,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ChangeState value) changeState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ChangeState value)? changeState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ChangeState value)? changeState,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ChangeThemeStateCopyWith<ChangeThemeState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChangeThemeStateCopyWith<$Res> {
  factory $ChangeThemeStateCopyWith(
          ChangeThemeState value, $Res Function(ChangeThemeState) then) =
      _$ChangeThemeStateCopyWithImpl<$Res, ChangeThemeState>;
  @useResult
  $Res call({TypeTheme typeTheme});
}

/// @nodoc
class _$ChangeThemeStateCopyWithImpl<$Res, $Val extends ChangeThemeState>
    implements $ChangeThemeStateCopyWith<$Res> {
  _$ChangeThemeStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? typeTheme = freezed,
  }) {
    return _then(_value.copyWith(
      typeTheme: freezed == typeTheme
          ? _value.typeTheme
          : typeTheme // ignore: cast_nullable_to_non_nullable
              as TypeTheme,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ChangeStateImplCopyWith<$Res>
    implements $ChangeThemeStateCopyWith<$Res> {
  factory _$$ChangeStateImplCopyWith(
          _$ChangeStateImpl value, $Res Function(_$ChangeStateImpl) then) =
      __$$ChangeStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({TypeTheme typeTheme});
}

/// @nodoc
class __$$ChangeStateImplCopyWithImpl<$Res>
    extends _$ChangeThemeStateCopyWithImpl<$Res, _$ChangeStateImpl>
    implements _$$ChangeStateImplCopyWith<$Res> {
  __$$ChangeStateImplCopyWithImpl(
      _$ChangeStateImpl _value, $Res Function(_$ChangeStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? typeTheme = freezed,
  }) {
    return _then(_$ChangeStateImpl(
      freezed == typeTheme
          ? _value.typeTheme
          : typeTheme // ignore: cast_nullable_to_non_nullable
              as TypeTheme,
    ));
  }
}

/// @nodoc

class _$ChangeStateImpl implements _ChangeState {
  const _$ChangeStateImpl(this.typeTheme);

  @override
  final TypeTheme typeTheme;

  @override
  String toString() {
    return 'ChangeThemeState.changeState(typeTheme: $typeTheme)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ChangeStateImpl &&
            const DeepCollectionEquality().equals(other.typeTheme, typeTheme));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(typeTheme));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ChangeStateImplCopyWith<_$ChangeStateImpl> get copyWith =>
      __$$ChangeStateImplCopyWithImpl<_$ChangeStateImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(TypeTheme typeTheme) changeState,
  }) {
    return changeState(typeTheme);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(TypeTheme typeTheme)? changeState,
  }) {
    return changeState?.call(typeTheme);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(TypeTheme typeTheme)? changeState,
    required TResult orElse(),
  }) {
    if (changeState != null) {
      return changeState(typeTheme);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ChangeState value) changeState,
  }) {
    return changeState(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ChangeState value)? changeState,
  }) {
    return changeState?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ChangeState value)? changeState,
    required TResult orElse(),
  }) {
    if (changeState != null) {
      return changeState(this);
    }
    return orElse();
  }
}

abstract class _ChangeState implements ChangeThemeState {
  const factory _ChangeState(final TypeTheme typeTheme) = _$ChangeStateImpl;

  @override
  TypeTheme get typeTheme;
  @override
  @JsonKey(ignore: true)
  _$$ChangeStateImplCopyWith<_$ChangeStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
