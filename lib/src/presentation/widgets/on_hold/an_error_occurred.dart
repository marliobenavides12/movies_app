import 'package:flutter/material.dart';

import 'package:material_symbols_icons/symbols.dart';

import 'package:movies_app/src/core/core_exports.dart' show AppColors;

class AnErrorOccurred extends StatelessWidget {
  final String message;

  const AnErrorOccurred({super.key, this.message = ''});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Icon(
            Symbols.error_circle_rounded_error,
            size: 40,
            grade: 200,
            weight: 700,
            opticalSize: 20,
            color: AppColors.error,
          ),
          const SizedBox(height: 10.0),
          const Text(
            'Ups ocurrio un error...',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 16,
              color: Colors.black45,
              fontWeight: FontWeight.w500,
            ),
          ),
          const SizedBox(height: 10.0),
          Text(
            'Message: $message',
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontSize: 16,
              color: Colors.black45,
              fontWeight: FontWeight.w500,
            ),
          ),
        ],
      ),
    );
  }
}
