import 'package:flutter/material.dart';

import 'package:material_symbols_icons/symbols.dart';

import 'package:movies_app/src/core/core_exports.dart' show AppColors;

class NoData extends StatelessWidget {
  const NoData({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Symbols.info,
            size: 40,
            grade: 200,
            weight: 700,
            opticalSize: 20,
            color: AppColors.primaryBlue,
          ),
          SizedBox(height: 10.0),
          Text(
            'Ups no hay datos cargados...',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 16,
              color: Colors.black45,
              fontWeight: FontWeight.w500,
            ),
          ),
        ],
      ),
    );
  }
}
