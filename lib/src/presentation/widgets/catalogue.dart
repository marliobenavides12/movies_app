import 'package:flutter/material.dart';

import 'package:shimmer/shimmer.dart';
import 'package:animate_do/animate_do.dart';

import 'package:movies_app/src/core/core_exports.dart';
import 'package:movies_app/src/presentation/widgets/widgets_exports.dart';
import 'package:movies_app/src/domain/domain_exports.dart' show MovieEntity;

class Catalogue extends StatelessWidget {
  final String catalogtitle;
  final List<MovieEntity> popularMovies;

  const Catalogue({
    super.key,
    required this.catalogtitle,
    required this.popularMovies,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 270,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 30, right: 30),
            child: Row(
              children: [
                Text(
                  catalogtitle,
                  style: const TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                const Spacer(),
                const Text('See all', style: TextStyle(color: Colors.white60)),
              ],
            ),
          ),
          Expanded(
            child: ListView.separated(
              shrinkWrap: true,
              itemCount: popularMovies.length,
              scrollDirection: Axis.horizontal,
              padding: const EdgeInsets.only(top: 20, left: 30),
              separatorBuilder: (BuildContext context, int index) =>
                  const SizedBox(width: 20),
              itemBuilder: (BuildContext context, int index) {
                return InkWell(
                  onTap: () {
                    context.push(
                      '/movieInfo/${popularMovies[index].id}',
                      extra: popularMovies[index].id,
                    );
                  },
                  child: SizedBox(
                    width: 120,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 180,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(20),
                            child: Image(
                              image:
                                  NetworkImage(popularMovies[index].posterPath),
                              loadingBuilder: (context, child,
                                      loadingProgress) =>
                                  loadingProgress != null
                                      ? Container(
                                          height: 180,
                                          clipBehavior: Clip.antiAlias,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                          ),
                                          child: Shimmer.fromColors(
                                            highlightColor: Colors.white,
                                            baseColor: Colors.grey.shade200,
                                            child:
                                                Container(color: Colors.white),
                                          ),
                                        )
                                      : FadeIn(
                                          duration: const Duration(seconds: 2),
                                          child: child,
                                        ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 8),
                        Text(
                          popularMovies[index].title,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(
                            fontSize: 13,
                            color: Colors.white,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        const SizedBox(height: 4),
                        RaitingBar(rating: popularMovies[index].voteAverage),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
