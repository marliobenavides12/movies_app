import 'package:flutter/material.dart';

import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'package:movies_app/src/core/core_exports.dart' show AppColors;

class RaitingBar extends StatelessWidget {
  final double rating;

  const RaitingBar({super.key, required this.rating});

  @override
  Widget build(BuildContext context) {
    return RatingBarIndicator(
      itemCount: 5,
      itemSize: 17,
      rating: (rating * 0.5),
      direction: Axis.horizontal,
      unratedColor: AppColors.yellowOpacity,
      itemPadding: const EdgeInsets.symmetric(horizontal: 1.0),
      itemBuilder: (context, _) => const Icon(
        Icons.star_outlined,
        color: AppColors.yellow,
      ),
    );
  }
}
