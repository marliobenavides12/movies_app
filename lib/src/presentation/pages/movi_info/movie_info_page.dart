import 'package:flutter/material.dart';

import 'package:material_symbols_icons/symbols.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:movies_app/src/core/core_exports.dart';
import 'package:movies_app/src/presentation/logic/bloc_exports.dart';
import 'package:movies_app/src/presentation/widgets/widgets_exports.dart';
import 'package:movies_app/src/presentation/pages/movi_info/widgets/widgets_movie_info_exports.dart';

class MovieInfoPage extends StatefulWidget {
  final int movieId;

  const MovieInfoPage({super.key, required this.movieId});

  @override
  State<MovieInfoPage> createState() => _MovieInfoPageState();
}

class _MovieInfoPageState extends State<MovieInfoPage> {
  @override
  void initState() {
    super.initState();

    Future.microtask(
      () => context
          .read<MovieInfoBloc>()
          .add(MovieInfoEvent.loadingData(widget.movieId)),
    );
  }

  void _goBack() => context.read<MovieInfoBloc>().add(
        const MovieInfoEvent.initialEvent(),
      );

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MovieInfoBloc, MovieInfoState>(
      builder: (context, state) {
        return WillPopScope(
          onWillPop: () async {
            _goBack();
            return true;
          },
          child: Scaffold(
            extendBody: true,
            extendBodyBehindAppBar: true,
            backgroundColor: Theme.of(context).colorScheme.secondary,
            appBar: AppBar(
              shadowColor: Colors.white,
              foregroundColor: Colors.white,
              forceMaterialTransparency: true,
              leading: IconButton(
                onPressed: () {
                  _goBack();
                  context.pop(true);
                },
                icon: const Icon(
                  Symbols.keyboard_backspace_rounded,
                  size: 30,
                  grade: 200,
                  weight: 300,
                  opticalSize: 48,
                  color: Colors.white,
                ),
              ),
              actions: [
                IconButton(
                  onPressed: () {},
                  icon: const FaIcon(
                    FontAwesomeIcons.heart,
                    size: 20,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
            body: state.maybeWhen(
              orElse: () => const SizedBox(),
              initialState: () => const Loading(),
              loadingState: () => const Loading(),
              noDataState: () => const NoData(),
              errorState: (errorMesage) =>
                  AnErrorOccurred(message: errorMesage),
              dataLoadedState: (movieDetail, movieActors) {
                return Column(
                  children: [
                    TopImage(imagePath: movieDetail.backdropPath),
                    ListInfo(movieActors: movieActors, movieDetail: movieDetail)
                  ],
                );
              },
            ),
          ),
        );
      },
    );
  }
}
