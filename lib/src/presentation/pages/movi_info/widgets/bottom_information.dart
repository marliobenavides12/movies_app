import 'package:flutter/material.dart';

class BottomInformation extends StatelessWidget {
  final String title;
  final double spaceValue;
  final List<String> list;

  const BottomInformation({
    super.key,
    required this.list,
    required this.title,
    this.spaceValue = 16.0,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: const TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(width: spaceValue),
        Expanded(
          child: Wrap(
            alignment: WrapAlignment.start,
            crossAxisAlignment: WrapCrossAlignment.start,
            children: List.generate(
              list.length,
              (i) => Text(
                (i + 1 == list.length) ? '${list[i]} ' : '${list[i]}, ',
                style: const TextStyle(color: Colors.white60),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
