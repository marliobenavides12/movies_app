import 'package:flutter/material.dart';

import 'package:shimmer/shimmer.dart';
import 'package:animate_do/animate_do.dart';

import 'package:movies_app/src/domain/domain_exports.dart'
    show MovieActorEntity;

class ListActors extends StatelessWidget {
  final List<MovieActorEntity> movieActors;

  const ListActors({super.key, required this.movieActors});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 110,
      child: ListView.separated(
        shrinkWrap: true,
        itemCount: movieActors.length,
        scrollDirection: Axis.horizontal,
        separatorBuilder: (context, index) => const SizedBox(width: 30),
        itemBuilder: (BuildContext context, int index) {
          return SizedBox(
            width: 60,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: 60,
                  height: 60,
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                  ),
                  child: Image(
                    fit: BoxFit.cover,
                    image: NetworkImage(movieActors[index].profilePath),
                    loadingBuilder: (context, child, loadingProgress) =>
                        loadingProgress != null
                            ? Container(
                                width: double.infinity,
                                clipBehavior: Clip.antiAlias,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                child: Shimmer.fromColors(
                                  highlightColor: Colors.white,
                                  baseColor: Colors.grey.shade200,
                                  child: Container(color: Colors.white),
                                ),
                              )
                            : FadeIn(
                                duration: const Duration(seconds: 2),
                                child: child,
                              ),
                  ),
                ),
                const SizedBox(height: 10.0),
                Text(
                  movieActors[index].name,
                  maxLines: 2,
                  textAlign: TextAlign.center,
                  style: const TextStyle(color: Colors.white60),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
