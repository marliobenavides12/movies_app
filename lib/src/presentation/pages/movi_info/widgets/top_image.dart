import 'package:flutter/material.dart';

import 'package:shimmer/shimmer.dart';
import 'package:animate_do/animate_do.dart';

class TopImage extends StatelessWidget {
  final String imagePath;

  const TopImage({super.key, required this.imagePath});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 280,
      width: double.infinity,
      child: ClipRRect(
        borderRadius: const BorderRadius.only(
          bottomLeft: Radius.circular(25),
          bottomRight: Radius.circular(25),
        ),
        child: Image(
          fit: BoxFit.cover,
          image: NetworkImage(imagePath),
          loadingBuilder: (context, child, loadingProgress) =>
              loadingProgress != null
                  ? Container(
                      height: 180,
                      width: double.infinity,
                      clipBehavior: Clip.antiAlias,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Shimmer.fromColors(
                        highlightColor: Colors.white,
                        baseColor: Colors.grey.shade200,
                        child: Container(color: Colors.white),
                      ),
                    )
                  : FadeIn(
                      duration: const Duration(seconds: 2),
                      child: child,
                    ),
        ),
      ),
    );
  }
}
