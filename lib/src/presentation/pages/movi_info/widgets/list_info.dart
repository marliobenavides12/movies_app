import 'package:flutter/material.dart';

import 'package:movies_app/src/domain/domain_exports.dart';
import 'package:movies_app/src/presentation/widgets/widgets_exports.dart'
    show RaitingBar;
import 'package:movies_app/src/presentation/pages/movi_info/widgets/widgets_movie_info_exports.dart';

class ListInfo extends StatelessWidget {
  final MovieDetailEntity movieDetail;
  final List<MovieActorEntity> movieActors;

  const ListInfo({
    super.key,
    required this.movieActors,
    required this.movieDetail,
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(30, 36, 30, 20),
        child: CustomScrollView(
          slivers: [
            SliverList(
              delegate: SliverChildBuilderDelegate(
                (context, index) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        movieDetail.title,
                        style: const TextStyle(
                          fontSize: 24,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const SizedBox(height: 10.0),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              color: const Color(0xFF6a737f),
                              borderRadius: BorderRadius.circular(50),
                            ),
                            padding: const EdgeInsets.symmetric(
                              vertical: 8.0,
                              horizontal: 20.0,
                            ),
                            child: const Text(
                              'WHATCH NOW',
                              style: TextStyle(
                                fontSize: 12,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          const Spacer(),
                          RaitingBar(rating: movieDetail.voteAverage),
                        ],
                      ),
                      const SizedBox(height: 30.0),
                      Text(
                        movieDetail.overview,
                        textAlign: TextAlign.left,
                        style: const TextStyle(
                          height: 1.8,
                          color: Colors.white60,
                        ),
                      ),
                      const SizedBox(height: 24.0),
                      ListActors(movieActors: movieActors),
                      const SizedBox(height: 18.0),
                      BottomInformation(
                        title: 'Studio',
                        spaceValue: 25,
                        list: movieDetail.productionCompanies
                            .map((e) => e.name)
                            .toList(),
                      ),
                      const SizedBox(height: 8.0),
                      BottomInformation(
                        title: 'Genre',
                        spaceValue: 26,
                        list: movieDetail.genres.map((e) => e.name).toList(),
                      ),
                      const SizedBox(height: 8.0),
                      BottomInformation(
                        title: 'Release',
                        list: [movieDetail.releaseDate.toString()],
                      ),
                    ],
                  );
                },
                childCount: 1,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
