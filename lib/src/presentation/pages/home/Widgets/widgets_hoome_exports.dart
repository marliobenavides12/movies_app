export 'view.dart';
export 'welcome_text.dart';
export 'movie_search.dart';
export 'change_theme.dart';
export 'popular_catalogs.dart';
export 'top_rated_catalogs.dart';
