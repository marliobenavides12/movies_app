import 'package:flutter/material.dart';

import 'package:material_symbols_icons/symbols.dart';

import 'package:movies_app/src/presentation/logic/bloc_exports.dart';
import 'package:movies_app/src/core/core_exports.dart' show TypeTheme;

class NewWidget extends StatelessWidget {
  const NewWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChangeThemeBloc, ChangeThemeState>(
      builder: (context, state) {
        return IconButton(
          onPressed: () => context
              .read<ChangeThemeBloc>()
              .add(ChangeThemeEvent.changeEvent(state.typeTheme)),
          icon: state.typeTheme == TypeTheme.light
              ? const Icon(
                  Symbols.clear_night_rounded,
                  size: 24,
                  grade: 200,
                  weight: 200,
                  opticalSize: 48,
                  color: Colors.white,
                )
              : const Icon(
                  Symbols.clear_day_rounded,
                  size: 24,
                  grade: 200,
                  weight: 200,
                  opticalSize: 48,
                  color: Colors.white,
                ),
        );
      },
    );
  }
}
