import 'package:flutter/material.dart';

import 'package:movies_app/src/domain/domain_exports.dart' show MovieEntity;
import 'package:movies_app/src/presentation/widgets/widgets_exports.dart'
    show Catalogue;

class TopRatedCatalogs extends StatelessWidget {
  final List<MovieEntity> topRatedMovies;

  const TopRatedCatalogs({super.key, required this.topRatedMovies});

  @override
  Widget build(BuildContext context) {
    return Catalogue(catalogtitle: 'TOP RATED', popularMovies: topRatedMovies);
  }
}
