import 'package:flutter/material.dart';

import 'package:movies_app/src/domain/domain_exports.dart' show MovieEntity;
import 'package:movies_app/src/presentation/pages/home/Widgets/widgets_hoome_exports.dart';

class HomeView extends StatelessWidget {
  final List<MovieEntity> popularMovies;
  final List<MovieEntity> topRatedMovies;

  const HomeView({
    super.key,
    required this.popularMovies,
    required this.topRatedMovies,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).colorScheme.primary,
      child: Column(
        children: [
          SafeArea(
            child: Container(
              width: double.infinity,
              padding: const EdgeInsets.fromLTRB(45, 15, 26, 26),
              child: const Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [WelcomeText(), MovieSearch()],
              ),
            ),
          ),
          Expanded(
            child: Container(
              clipBehavior: Clip.antiAliasWithSaveLayer,
              decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.secondary,
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(25),
                  topRight: Radius.circular(25),
                ),
              ),
              child: CustomScrollView(
                slivers: [
                  SliverList(
                    delegate: SliverChildBuilderDelegate(
                      (context, index) {
                        return Column(
                          children: [
                            const SizedBox(height: 36.0),
                            PopularCatalogs(popularMovies: popularMovies),
                            const SizedBox(height: 20.0),
                            TopRatedCatalogs(topRatedMovies: topRatedMovies),
                            const SizedBox(height: 36.0),
                          ],
                        );
                      },
                      childCount: 1,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
