import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MovieSearch extends StatelessWidget {
  const MovieSearch({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        color: Theme.of(context).colorScheme.background.withOpacity(0.24),
      ),
      margin: const EdgeInsets.only(top: 20),
      padding: const EdgeInsets.symmetric(vertical: 6.0, horizontal: 20),
      child: const Row(
        children: [
          FaIcon(
            FontAwesomeIcons.magnifyingGlass,
            size: 14,
            color: Colors.white,
          ),
          SizedBox(width: 10.0),
          Text('Search', style: TextStyle(color: Colors.white60)),
        ],
      ),
    );
  }
}
