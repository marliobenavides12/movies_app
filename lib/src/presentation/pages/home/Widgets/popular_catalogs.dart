import 'package:flutter/material.dart';

import 'package:movies_app/src/domain/domain_exports.dart' show MovieEntity;
import 'package:movies_app/src/presentation/widgets/widgets_exports.dart'
    show Catalogue;

class PopularCatalogs extends StatelessWidget {
  final List<MovieEntity> popularMovies;

  const PopularCatalogs({super.key, required this.popularMovies});

  @override
  Widget build(BuildContext context) {
    return Catalogue(
      popularMovies: popularMovies,
      catalogtitle: 'RECOMMENDED FOR YOU',
    );
  }
}
