import 'package:flutter/material.dart';

import 'package:movies_app/src/presentation/logic/bloc_exports.dart';
import 'package:movies_app/src/presentation/widgets/widgets_exports.dart';
import 'package:movies_app/src/presentation/pages/home/Widgets/widgets_hoome_exports.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();

    Future.microtask(
      () => context.read<HomeBloc>().add(const HomeEvent.loadingData()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      builder: (context, state) {
        return Scaffold(
          backgroundColor: Theme.of(context).colorScheme.primary,
          appBar: AppBar(
            elevation: 0.0,
            shadowColor: Colors.white,
            leading: const NewWidget(),
            foregroundColor: Colors.white,
            forceMaterialTransparency: true,
            backgroundColor: Colors.transparent,
          ),
          body: state.maybeWhen(
            orElse: () => const SizedBox(),
            initialState: () => const Loading(),
            loadingState: () => const Loading(),
            noDataState: () => const NoData(),
            dataLoadedState: (popularMovies, topRatedMovies) => HomeView(
              popularMovies: popularMovies,
              topRatedMovies: topRatedMovies,
            ),
            errorState: (errorMesage) => AnErrorOccurred(message: errorMesage),
          ),
        );
      },
    );
  }
}
