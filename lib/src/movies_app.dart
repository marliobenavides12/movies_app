import 'package:flutter/material.dart';

import 'package:movies_app/src/core/core_exports.dart';
import 'package:movies_app/src/injection_container.dart';
import 'package:movies_app/src/presentation/logic/bloc_exports.dart';

class MoviesApp extends StatelessWidget {
  const MoviesApp({super.key});

  // This widget is the root of application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<HomeBloc>(create: (context) => sl<HomeBloc>()),
        BlocProvider<MovieInfoBloc>(create: (context) => sl<MovieInfoBloc>()),
        BlocProvider<ChangeThemeBloc>(
            create: (context) => sl<ChangeThemeBloc>()),
      ],
      child: BlocBuilder<ChangeThemeBloc, ChangeThemeState>(
        builder: (context, state) {
          return MaterialApp.router(
            title: 'Movies App',
            debugShowCheckedModeBanner: false,
            theme: state.typeTheme == TypeTheme.light
                ? AppTheme.lightTheme
                : AppTheme.darkTheme,
            routerConfig: sl<RouterConfig<Object>>(),
          );
        },
      ),
    );
  }
}
