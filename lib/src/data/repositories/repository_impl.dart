import 'package:dartz/dartz.dart';

import 'package:movies_app/src/data/data_exports.dart';
import 'package:movies_app/src/domain/domain_exports.dart';

class RepositoryImpl implements Repository {
  final DataSource _dataSource;

  RepositoryImpl({required DataSource dataSource}) : _dataSource = dataSource;

  @override
  Future<Either<String, List<MovieEntity>>> getPopularMovies() async {
    try {
      final response = await _dataSource.getPopularMovies();
      final convert = List<MovieEntity>.from(
        response.map((x) => MapperMovie.modelToEntity(x)),
      );
      return right(convert);
    } catch (e) {
      return left(e.toString());
    }
  }

  @override
  Future<Either<String, List<MovieEntity>>> getTopRatedMovies() async {
    try {
      final response = await _dataSource.getTopRatedMovies();
      final convert = List<MovieEntity>.from(
        response.map((x) => MapperMovie.modelToEntity(x)),
      );
      return right(convert);
    } catch (e) {
      return left(e.toString());
    }
  }

  @override
  Future<Either<String, MovieDetailEntity>> getMovieDetail(
    int movieId,
  ) async {
    try {
      final response = await _dataSource.getMovieDetail(movieId);
      final convert = MapperMovieDetail.modelToEntity(response);

      return right(convert);
    } catch (e) {
      return left(e.toString());
    }
  }

  @override
  Future<Either<String, List<MovieActorEntity>>> getMovieActors(
    int movieId,
  ) async {
    try {
      final response = await _dataSource.getMovieActors(movieId);
      final convert = List<MovieActorEntity>.from(
        response.map((x) => MapperMovieActor.modelToEntity(x)),
      );
      return right(convert);
    } catch (e) {
      return left(e.toString());
    }
  }
}
