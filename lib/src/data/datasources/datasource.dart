import 'package:dio/dio.dart';

import 'package:movies_app/src/data/data_exports.dart';
import 'package:movies_app/src/core/core_exports.dart' show Env;

abstract class DataSource {
  Future<List<MovieModel>> getPopularMovies();
  Future<List<MovieModel>> getTopRatedMovies();
  Future<MovieDetailModel> getMovieDetail(int movieId);
  Future<List<MovieActorModel>> getMovieActors(int movieId);
}

class DataSourceImpl implements DataSource {
  final Dio _dio;

  DataSourceImpl(this._dio);

  @override
  Future<List<MovieModel>> getPopularMovies() async {
    final response = await _dio.get(
      Env.moviePopularUrl,
      queryParameters: {'language': Env.paramLanguageEs},
      options: Options(
        headers: {
          "accept": "application/json",
          "Authorization": "Bearer ${Env.accessToken}"
        },
      ),
    );
    final List<MovieModel> populars = (response.data["results"] as List)
        .map((res) => MovieModel.fromJson(res as Map<String, dynamic>))
        .toList();
    return populars;
  }

  @override
  Future<List<MovieModel>> getTopRatedMovies() async {
    final response = await _dio.get(
      Env.movieTopRatedUrl,
      queryParameters: {'language': Env.paramLanguageEs},
      options: Options(
        headers: {
          "accept": "application/json",
          "Authorization": "Bearer  ${Env.accessToken}"
        },
      ),
    );
    final List<MovieModel> topRated = (response.data["results"] as List)
        .map((res) => MovieModel.fromJson(res as Map<String, dynamic>))
        .toList();
    return topRated;
  }

  @override
  Future<MovieDetailModel> getMovieDetail(int movieId) async {
    final response = await _dio.get(
      '${Env.movieApiUrl}$movieId',
      queryParameters: {'language': Env.paramLanguageEs},
      options: Options(
        headers: {
          "accept": "application/json",
          "Authorization": "Bearer  ${Env.accessToken}"
        },
      ),
    );
    final MovieDetailModel movieDetail =
        MovieDetailModel.fromJson(response.data);
    return movieDetail;
  }

  @override
  Future<List<MovieActorModel>> getMovieActors(int movieId) async {
    final response = await _dio.get(
      '${Env.movieApiUrl}$movieId/credits',
      queryParameters: {'language': Env.paramLanguageEs},
      options: Options(
        headers: {
          "accept": "application/json",
          "Authorization": "Bearer  ${Env.accessToken}"
        },
      ),
    );
    final List<MovieActorModel> actors = (response.data["cast"] as List)
        .map((res) => MovieActorModel.fromJson(res as Map<String, dynamic>))
        .toList();
    return actors;
  }
}
