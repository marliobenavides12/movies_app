import 'package:movies_app/src/data/data_exports.dart';
import 'package:movies_app/src/domain/domain_exports.dart';

class MapperMovieDetail {
  static MovieDetailEntity modelToEntity(MovieDetailModel movieDetail) =>
      MovieDetailEntity(
        id: movieDetail.id,
        title: movieDetail.title,
        overview: movieDetail.overview,
        posterPath: movieDetail.posterPath,
        releaseDate: movieDetail.releaseDate,
        voteAverage: movieDetail.voteAverage,
        backdropPath: movieDetail.backdropPath,
        originalTitle: movieDetail.originalTitle,
        genres: List<GenreEntity>.from(
          movieDetail.genres.map((x) => modelToGenreEntity(x)),
        ),
        productionCompanies: List<ProductionCompanyEntity>.from(
          movieDetail.productionCompanies
              .map((x) => modelToProductionCompanyEntity(x)),
        ),
      );

  static ProductionCompanyEntity modelToProductionCompanyEntity(
          ProductionCompanyModel productionCompanies) =>
      ProductionCompanyEntity(
        id: productionCompanies.id,
        name: productionCompanies.name,
        originCountry: productionCompanies.originCountry,
      );

  static GenreEntity modelToGenreEntity(GenreModel genres) => GenreEntity(
        id: genres.id,
        name: genres.name,
      );
}
