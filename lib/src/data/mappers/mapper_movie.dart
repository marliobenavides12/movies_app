import 'package:movies_app/src/data/data_exports.dart' show MovieModel;
import 'package:movies_app/src/domain/domain_exports.dart' show MovieEntity;

class MapperMovie {
  static MovieEntity modelToEntity(MovieModel popularMovie) => MovieEntity(
        id: popularMovie.id,
        title: popularMovie.title,
        posterPath: popularMovie.posterPath,
        voteAverage: popularMovie.voteAverage,
        backdropPath: popularMovie.backdropPath,
        originalTitle: popularMovie.originalTitle,
      );
}
