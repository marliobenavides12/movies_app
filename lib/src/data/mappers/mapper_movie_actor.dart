import 'package:movies_app/src/data/models/movie_actor_model.dart';
import 'package:movies_app/src/domain/entities/actor_entity.dart'
    show MovieActorEntity;

class MapperMovieActor {
  static MovieActorEntity modelToEntity(MovieActorModel movieActor) =>
      MovieActorEntity(
        id: movieActor.id,
        name: movieActor.name,
        character: movieActor.character,
        profilePath: movieActor.profilePath,
      );
}
