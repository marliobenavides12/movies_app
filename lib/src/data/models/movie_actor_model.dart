import 'package:movies_app/src/core/core_exports.dart' show AppFunctions;

class MovieActorModel {
  final int id;
  final String name;
  final String originalName;
  final String profilePath;
  final String character;

  MovieActorModel({
    required this.id,
    required this.name,
    required this.originalName,
    required this.profilePath,
    required this.character,
  });

  factory MovieActorModel.fromJson(Map<String, dynamic> json) =>
      MovieActorModel(
        id: json["id"],
        name: json["name"],
        originalName: json["original_name"],
        profilePath: AppFunctions.validateImage(json["profile_path"]),
        character: json["character"],
      );
}
