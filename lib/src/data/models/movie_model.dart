import 'package:movies_app/src/core/core_exports.dart' show AppFunctions;

class MovieModel {
  final int id;
  final String title;
  final String posterPath;
  final double voteAverage;
  final String backdropPath;
  final String originalTitle;

  MovieModel({
    required this.id,
    required this.title,
    required this.posterPath,
    required this.voteAverage,
    required this.backdropPath,
    required this.originalTitle,
  });

  factory MovieModel.fromJson(Map<String, dynamic> json) => MovieModel(
        id: json["id"],
        title: json["title"],
        backdropPath: json["backdrop_path"],
        originalTitle: json["original_title"],
        voteAverage: json["vote_average"].toDouble(),
        posterPath: AppFunctions.validateImage(json["poster_path"]),
      );
}
