import 'package:movies_app/src/core/core_exports.dart' show AppFunctions;

class MovieDetailModel {
  final int id;
  final String title;
  final String overview;
  final String posterPath;
  final double voteAverage;
  final String releaseDate;
  final String backdropPath;
  final String originalTitle;
  final List<GenreModel> genres;
  final List<ProductionCompanyModel> productionCompanies;

  MovieDetailModel({
    required this.id,
    required this.title,
    required this.genres,
    required this.overview,
    required this.posterPath,
    required this.voteAverage,
    required this.releaseDate,
    required this.backdropPath,
    required this.originalTitle,
    required this.productionCompanies,
  });

  factory MovieDetailModel.fromJson(Map<String, dynamic> json) =>
      MovieDetailModel(
        id: json["id"],
        title: json["title"],
        overview: json["overview"],
        originalTitle: json["original_title"],
        voteAverage: json["vote_average"].toDouble(),
        genres: List<GenreModel>.from(
          json["genres"].map((x) => GenreModel.fromJson(x)),
        ),
        productionCompanies: List<ProductionCompanyModel>.from(
          json["production_companies"]
              .map((x) => ProductionCompanyModel.fromJson(x)),
        ),
        releaseDate: AppFunctions.formatDate(json["release_date"]),
        posterPath: AppFunctions.validateImage(json["poster_path"]),
        backdropPath: AppFunctions.validateImage(json["backdrop_path"]),
      );
}

class GenreModel {
  final int id;
  final String name;

  GenreModel({required this.id, required this.name});

  factory GenreModel.fromJson(Map<String, dynamic> json) => GenreModel(
        id: json["id"],
        name: json["name"],
      );
}

class ProductionCompanyModel {
  final int id;
  final String name;
  final String originCountry;

  ProductionCompanyModel({
    required this.id,
    required this.name,
    required this.originCountry,
  });

  factory ProductionCompanyModel.fromJson(Map<String, dynamic> json) =>
      ProductionCompanyModel(
        id: json["id"],
        name: json["name"],
        originCountry: json["origin_country"],
      );
}
