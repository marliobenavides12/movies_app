import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:movies_app/src/movies_app.dart';
import 'package:movies_app/src/injection_container.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initDependencies();
  await dotenv.load(fileName: '.env.dev');

  runApp(const MoviesApp());
}
